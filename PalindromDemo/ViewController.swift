//
//  ViewController.swift
//  PalindromDemo
//
//  Created by Hardik Solanki on 12/8/17.
//  Copyright © 2017 ShivLab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var textFieldValue: UITextField!
    @IBOutlet weak var lableResult: UILabel!
    
    //MARK: viewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    //MARK: Button Actions
    @IBAction func buttonCheck(_ sender: UIButton) {
        
        // For remove whitespace
        var formattedString = textFieldValue.text?.replacingOccurrences(of: " ", with: "")
        formattedString = formattedString?.lowercased()
        
        if((formattedString) != "") {
            
            // Check Palindrom
            checkPalindrom(checkString: formattedString!)
            
        } else {
            
            lableResult.text = "Enter Any Values"
            
        }
        
    }
    
    //MARK: Check Palindrom Using Reversed Function
    func checkPalindrom(checkString: String) {
        
        // Check Palindrom Using Reversed Function
        let revstring = String(checkString.characters.reversed())
        
        // Check Palindrom Using Character by Character
        
        /*var revstring: String = ""
         for character in checkString.characters {
         revstring = String(character) + revstring
         } */
        
        
        if (revstring == checkString) {
            
            lableResult.text = "\(checkString) is a palindrome"
            
        } else {
            
            lableResult.text = "\(checkString) is not a palindrome"
        }
    }
    
    
}

